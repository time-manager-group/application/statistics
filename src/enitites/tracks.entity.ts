import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import {TrackTypes} from "./track-types.entity";

@Entity()
export class Tracks {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({name: 'date_start'})
    dateStart: string;

    @Column({name: 'date_end'})
    dateEnd: string;

    @Column({name: 'type_id'})
    typeId: number;

    @Column({default: true, name: 'is_active'})
    isActive: boolean;

    @ManyToOne(() => TrackTypes, trackTypes => trackTypes.tracks)
    @JoinColumn([
        { name: "type_id", referencedColumnName: "id" },
    ])
    type: TrackTypes;
}
