import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import {Tracks} from "./tracks.entity";

@Entity()
export class TrackTypes {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @OneToMany(() => Tracks, tracks => tracks.type)
    tracks: Tracks[];
}
