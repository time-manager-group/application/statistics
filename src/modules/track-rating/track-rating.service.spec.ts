import { Test, TestingModule } from '@nestjs/testing';
import { TrackRatingService } from './track-rating.service';

describe('TrackRatingService', () => {
  let service: TrackRatingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TrackRatingService],
    }).compile();

    service = module.get<TrackRatingService>(TrackRatingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
