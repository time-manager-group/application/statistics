import {Injectable} from '@nestjs/common';
import {Tracks} from "../../enitites/tracks.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Brackets, getConnection, Repository, SelectQueryBuilder} from "typeorm";
import {FilterDto} from "./dto/filter.dto";
import * as moment from 'moment';

@Injectable()
export class TrackRatingService {
    constructor(
        @InjectRepository(Tracks)
        private tracksRepository: Repository<Tracks>,
    ) {
    }

    async getSumAll(filter: FilterDto | null) {
        let countAll = await this.getCountAll(filter);
        const timeAll = await this.getTimeAll(filter);

        const map = new Map();

        for (const countItem of countAll) {
            map.set(countItem.type, countItem)
        }
        // @ts-ignore
        for (const timeItem of timeAll) {
            map.set(timeItem.type, {
                ...map.get(timeItem.type),
                ...timeItem
            })
        }
        return Array.from(map).map(([type, value]) => ({type, ...value}));
    }

    private async getCountAll(filter: FilterDto | null) {
        let where: Array<string> = [];
        let params: {  } = {  };
        if (filter.getTypesArrayList()) {
            const inParamsArray: {} = {};

            let i = 1;
            for (const param of filter.getTypesArrayList()) {
                inParamsArray['code' + i] = param;
                i++;
            }

            where.push('track_types.code IN (:' + Object.keys(inParamsArray).join(', :') + ')');
            for (const key in inParamsArray) {
                params[key] = inParamsArray[key];
            }
        }

        if (filter.searchText) {
            if (filter.activeSearchOperators) {
                TrackRatingService.buildLikeCondition(where, filter.searchText.toString(), params);
            } else {
                where.push('tracks.name LIKE :searchText');
                params['searchText'] = '%' + filter.searchText + '%';
            }
        }

        if (filter.dateFrom) {
            where.push('tracks.date_start >= :dateStart');
            params['dateStart'] = moment(filter.dateFrom).format('YYYY-MM-DD HH:mm:ss');
        }

        if (filter.dateTo) {
            where.push('tracks.date_end <= :dateEnd');
            params['dateEnd'] = moment(filter.dateTo).format('YYYY-MM-DD HH:mm:ss');
        }

        const whereString = where.length ? ('where ' + where.join(' and ')) : '';

        const [query,parameters] = getConnection().driver.escapeQueryWithParameters(`
            SELECT COUNT(tasks.name)   tasks_count,
                   track_types.code as type
            from (SELECT tracks.name, type_id
                  from tracks
                           left join track_types on tracks.type_id = track_types.id
                      ${whereString}
                  group by name, type_id) tasks
                     left join track_types on tasks.type_id = track_types.id
            group by type_id
            order by type_id desc
        `, params, {});
        // noinspection SqlNoDataSourceInspection
        return await this.tracksRepository.query(query,parameters);

    }

    private async getTimeAll(filter: FilterDto | null): Promise<Tracks[]> {
        const queryBuilder = this.tracksRepository
            .createQueryBuilder('tracks')
            .leftJoinAndSelect('tracks.type', 'type')
            .select([
                'type.code as type',
                'SUM(TIMESTAMPDIFF(SECOND, tracks.date_start, CASE\n' +
                '    WHEN  tracks.is_active = 1\n' +
                '        THEN CONVERT_TZ(NOW(), \'SYSTEM\', \'UTC\')\n' +
                '    ELSE tracks.date_end\n' +
                'END)) as all_seconds',
            ])
            .addGroupBy('tracks.type_id')
            .addOrderBy('tracks.type_id', 'DESC');

        TrackRatingService.injectBaseFilter(queryBuilder, filter);

        return await
            queryBuilder.getRawMany();
    }

    async getAll(filter: FilterDto | null): Promise<Tracks[]> {

        const queryBuilder = this.tracksRepository.createQueryBuilder('tracks')
            .leftJoinAndSelect('tracks.type', 'type')
            .select([
                'type.code as type',
                'tracks.name as name',
                'SUM(TIMESTAMPDIFF(SECOND, tracks.date_start, CASE\n' +
                '    WHEN  tracks.is_active = 1\n' +
                '        THEN CONVERT_TZ(NOW(), \'SYSTEM\', \'UTC\')\n' +
                '    ELSE tracks.date_end\n' +
                'END)) as all_seconds',
            ])
            .addGroupBy('tracks.name')
            .addGroupBy('tracks.type_id')
            .addOrderBy('all_seconds', 'DESC')

        TrackRatingService.injectBaseFilter(queryBuilder, filter);

        if (filter.limit) {
            queryBuilder.limit(filter.limit);

        }

        return await queryBuilder.getRawMany();
    }

    private static injectBaseFilter(queryBuilder: SelectQueryBuilder<Tracks>, filter: FilterDto | null): void {
        if (filter.getTypesArrayList()) {
            queryBuilder.andWhere("type.code IN (:codes)", {codes: filter.getTypesArrayList()});
        }

        if (filter.searchText) {
            if (filter.activeSearchOperators) {
                TrackRatingService.buildLikeCondition(queryBuilder, filter.searchText.toString());
            } else {
                queryBuilder.andWhere('tracks.name LIKE :text', {text: `%${filter.searchText}%`});
            }
        }

        if (filter.dateFrom) {
            queryBuilder.andWhere(`tracks.date_start >= :dateStart`, {dateStart: moment(filter.dateFrom).format('YYYY-MM-DD HH:mm:ss')});
        }

        if (filter.dateTo) {
            queryBuilder.andWhere(`tracks.date_end <= :dateEnd`, {dateEnd: moment(filter.dateTo).format('YYYY-MM-DD HH:mm:ss')});
        }
    }

    private static buildLikeCondition(queryBuilder: SelectQueryBuilder<Tracks> | Array<string>, text: string, params : {} = {}) {
        text = text.trim();

        const arr = [];
        let iterable = 0;
        for (const word of text.split('')) {
            if (arr[iterable] == undefined) {
                arr[iterable] = {condition: null, text: '', not: false};
            }
            if (['|', '&'].includes(word)) {
                iterable++;
                arr[iterable] = {condition: word, text: '', not: false};
            } else if (['!'].includes(word)) {
                arr[iterable].not = true;
            } else {
                arr[iterable].text += word;
            }

        }

        if (arr.length) {
            if (queryBuilder instanceof SelectQueryBuilder) {
                queryBuilder.andWhere(new Brackets(qb => {
                    let key = 0;
                    for (const value of arr) {
                        const not: string = value.not ? ' NOT' : '';
                        const where: string = `tracks.name${not} LIKE :text${key}`;
                        const parameters = {};
                        parameters[`text${key}`] = `%${value.text}%`;
                        if (value.condition === '|') {
                            qb.orWhere(where, parameters);
                        } else if (value.condition === '&') {
                            qb.andWhere(where, parameters);
                        } else {
                            qb.where(where, parameters);
                        }
                        key++;
                    }

                }));
            } else {
                let where = '';
                let i = 1;
                for (const value of arr) {
                    const not: string = value.not ? ' NOT' : '';
                    const whereOne: string = `tracks.name${not} LIKE :text${i}`;
                    params[`text${i}`] = `%${value.text}%`;
                    if (value.condition === '|') {
                        where += ' OR ' + whereOne;
                    } else if (value.condition === '&') {
                        where += ' AND ' + whereOne;
                    } else {
                        where += whereOne;
                    }
                    i++;
                }
                queryBuilder.push(`(${where})`);
            }
        }
    }
}
