export interface TypesDto {
    useful: boolean | null;
    useless: boolean | null;
    undefined: boolean | null;
}
