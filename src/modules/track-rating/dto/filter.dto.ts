import {Request} from 'express';
import {TypesDto} from "./types.dto";


export class FilterDto {
    public types: TypesDto | null;
    public searchText: string | null;
    public activeSearchOperators: boolean;
    public dateFrom: Date | null;
    public dateTo: Date | null;
    public limit: number | null;

    constructor(request: Request) {
        const query = request.query;

        this.types = query.types ? JSON.parse(query.types.toString()) : null;
        this.searchText = query.searchText ? query.searchText.toString() : null;
        this.activeSearchOperators = query.activeSearchOperators ? !!parseInt(query.activeSearchOperators.toString()) : false;
        this.dateFrom = query.dateFrom ? new Date(query.dateFrom.toString()) : null;
        this.dateTo = query.dateTo ? new Date(query.dateTo.toString()) : null;
        this.limit = query.limit ? parseInt(query.limit.toString()) : null;
    }

    getTypesArrayList(): Array<string> | null {
        if (this.types === null) {
            return null;
        }
        const list = [];

        for (const listKey in this.types) {
            if (this.types[listKey] === true) {
                list.push(listKey);
            }

        }
        if (list.length === 0) {
            return null;
        }

        return list;
    }
}

