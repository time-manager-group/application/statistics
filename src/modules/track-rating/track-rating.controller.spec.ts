import { Test, TestingModule } from '@nestjs/testing';
import { TrackRatingController } from './track-rating.controller';

describe('TrackRatingController', () => {
  let controller: TrackRatingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrackRatingController],
    }).compile();

    controller = module.get<TrackRatingController>(TrackRatingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
