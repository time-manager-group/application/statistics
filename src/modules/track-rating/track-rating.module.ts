import {Module} from '@nestjs/common';
import {TrackRatingController} from './track-rating.controller';
import {TrackRatingService} from './track-rating.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Tracks} from "../../enitites/tracks.entity";
import {TrackTypes} from "../../enitites/track-types.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([Tracks, TrackTypes]),
    ],
    controllers: [TrackRatingController],
    providers: [TrackRatingService],
})
export class TrackRatingModule {
}
