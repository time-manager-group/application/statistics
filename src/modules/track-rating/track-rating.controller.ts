import {Controller, Get, Req} from '@nestjs/common';
import {TrackRatingService} from "./track-rating.service";
import {Request} from 'express';
import {FilterDto} from "./dto/filter.dto";

@Controller('track-rating')
export class TrackRatingController {
    constructor(private readonly taskerService: TrackRatingService) {
    }

    @Get('sum-all')
    getCountAll(@Req() request: Request) {
        return this.taskerService.getSumAll(new FilterDto(request));
    }

    @Get()
    getAll(@Req() request: Request) {
        return this.taskerService.getAll(new FilterDto(request));
    }
}
