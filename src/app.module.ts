import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AppGateway} from './app.gateway';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Tracks} from "./enitites/tracks.entity";
import {TrackTypes} from "./enitites/track-types.entity";
import { TrackRatingModule } from './modules/track-rating/track-rating.module';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.DB_HOST,
            port: +process.env.DB_PORT,
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE,
            entities: [Tracks, TrackTypes],
            synchronize: false,
        }),
        TrackRatingModule
    ],
    controllers: [AppController],
    providers: [AppService, AppGateway],
})
export class AppModule {
}
